import java.util.Optional;

public class SimpleMap<K, V> {
	@SuppressWarnings("unchecked")
	Node<K, V>[] table = (Node<K, V>[])new Node[10];
	int size;
	class Node<K, V> {
		K key;
		V value;
		public Node(K key, V value) {
			this.key = key;
			this.value = value;
		}
	}
	
	public void put(K key, V value) {
		table[size] = new Node<K, V>(key, value);
		size++;
	}
	
	public V get(K key) {
		Node<K, V> node = findNodeByKey(key);
		return node == null ? null : node.value;
	}
	
	public void merge(K key, V value, 
						MyFuncInterface<V, V, V> myFuncInterface) {
		Node<K, V> node = findNodeByKey(key);
		if (node == null)
			return;
		node.value = myFuncInterface.apply(node.value, value);
	}

	private Node<K, V> findNodeByKey(K key) {
		for (int i = 0; i < size; i++) {
			if (table[i].key == key)
				return table[i];
		}
		return null;
	}
	
	public V getOrDefault(K key, V defaultValue) {
		Node<K, V> node = findNodeByKey(key);
		if (node == null)
			return defaultValue;
		return node.value;
	}
	
	public Optional<V> getOptional(K key) {
		Node<K, V> node = findNodeByKey(key);
		return node == null ? Optional.empty() : Optional.ofNullable(node.value);
	}
}
