import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

public class DigitalOccurrencesAppl {

	public static void main(String[] args) {
		Map<String, Integer> map = new HashMap<>();
		
		map.put("Hello", 1);
		map.put("Hi", 2);
		map.put("Hello world", 3);
		
		
		Set<Entry<String, Integer>> setNodes = map.entrySet();
		
		List<Entry<String, Integer>> list = new ArrayList<>(setNodes);
		
		list.sort((o1, o2) -> o2.getValue() - o1.getValue());
		
//		list.forEach(t -> System.out.println(t.getKey() + " : " + t.getValue()));
		list.forEach(t -> System.out.println(t.toString()));
		
//		for (Entry<String, Integer> entry : list) {
//			System.out.println(entry.getKey() + " : " + entry.getValue());
//		}
		
		Map<String, Integer> map1 = Map.of("Hello", 1, "Hi", 2);
		
		
//		map1.forEach((k, v) -> System.out.println(k + " : " + v));
	}

}
