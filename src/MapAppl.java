import java.util.Map;

public class MapAppl {
	public static void main(String[] args) {
		Map<String, Integer> map = Map.of("Hello", 1, "Hi", 2);
		
		System.out.println(map.getOrDefault("Hello", 0));
		System.out.println(map.getOrDefault("Helo", 0));
	}
}