
public class OptionalSimpleMapAppl {

	public static void main(String[] args) {
		SimpleMap<String, Integer> map = new SimpleMap<>();
		map.put("Hello", 1);
		System.out.println(map.get("Hello"));

		Integer i = map.getOptional("Helo").orElse(-999);
		System.out.println(i);
		i = map.getOptional("Hello").orElse(-999);
		System.out.println(i);
		i = map.getOptional("Hello").orElseThrow(() -> new ArrayIndexOutOfBoundsException());
	}

}
