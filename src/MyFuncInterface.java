@FunctionalInterface
public interface MyFuncInterface<R, F, S> {
	R apply(F oldValue, S newValue);
}
